#!/bin/bash
# Job name:
#SBATCH --job-name=urbanDAS
#
# Partition:
#SBATCH --partition=lr6
#
# QoS:
#SBATCH --qos=lr_normal
#
#
# Processors:
#SBATCH --nodes=35
#SBATCH --ntasks=560
#SBATCH --ntasks-per-node=16
#
# Wall clock limit:
#SBATCH --time=01:00:00
#
# Mail type:
#SBATCH --mail-type=all
#
# Mail user:
#SBATCH --mail-user=dbin@lbl.gov

#SBATCH -o urbanDAS.%j.out
#SBATCH -e urbanDAS.%j.err
## Run command
module load gcc/7.4.0
module load openmpi
module load hdf5/1.10.5-gcc-p
module load fftw/3.3.8-gcc
module load boost

mpirun -n 560 /global/home/users/dbin/arrayudf-test/examples/das/das-fft-full-omp -d -c urbanDAS.config
