#include <mpi.h>
#include "ga.h"
#include <assert.h> /* assert */
#include <stdlib.h>

#define SIZE 16
#define CHUNK 4
#define READ_GHOST 1

int main(int argc, char **argv)
{
  MPI_Init(&argc, &argv);
  GA_Initialize();

  int me = GA_Nodeid();
  int nproc = GA_Nnodes();
  if (me == 0)
  {
    printf("Running on %d processors\n\n", nproc);
  }

  int ndim = 2;

  int64_t dims[2];
  dims[0] = SIZE;
  dims[1] = SIZE;

  int64_t chunk[2];
  chunk[0] = CHUNK;
  chunk[1] = CHUNK;

  char *array_name = (char *)"array A";
  int g_A = NGA_Create64(C_INT, ndim, dims, array_name, chunk);

  int64_t A_dist_lo[2], A_dist_hi[2];
  NGA_Distribution64(g_A, me, &(A_dist_lo[0]), &(A_dist_hi[0]));
  if (!me)
    printf("Ater create A & B:  A: ga_rank=%d, dist_lo=(%lld,%lld), dist_hi=(%lld,%lld)\n", me, A_dist_lo[0], A_dist_lo[1], A_dist_hi[0], A_dist_hi[1]);
  fflush(stdout);

  int64_t A_read_lo[2], A_read_hi[2], A_write_lo[2], A_write_hi[2], A_rw_ld[2];
  int *A_read_data, *A_write_data;
  int A_read_data_size = 1, A_write_data_size = 1;

  for (int j = 0; j < ndim; j++)
  {
    if (A_dist_hi[j] + 1 < SIZE)
    {
      A_read_hi[j] = A_dist_hi[j] + 1;
    }
    else
    {
      A_read_hi[j] = A_dist_hi[j];
    }

    if (A_dist_lo[j] - 1 >= 0)
    {
      A_read_lo[j] = A_dist_lo[j] - 1;
    }
    else
    {
      A_read_lo[j] = A_dist_lo[j];
    }

    A_write_hi[j] = A_dist_hi[j];
    A_write_lo[j] = A_dist_lo[j];

    A_read_data_size = A_read_data_size * (A_read_hi[j] - A_read_lo[j] + 1);
    A_write_data_size = A_write_data_size * (A_write_hi[j] - A_write_lo[j] + 1);
  }

  A_write_data = (int *)malloc(A_write_data_size * sizeof(int));
  A_read_data = (int *)malloc(A_read_data_size * sizeof(int));

  for (int i = 0; i < A_write_data_size; i++)
  {
    A_write_data[i] = i + me;
  }
  //Write Data to A;
  A_rw_ld[0] = A_dist_hi[0] - A_dist_lo[0] + 1;
  A_rw_ld[1] = A_dist_hi[1] - A_dist_lo[1] + 1;

  NGA_Put64(g_A, A_write_lo, A_write_hi, A_write_data, A_rw_ld);
  GA_Sync();

  if (me <= 1)
  {
    printf("Read on rank %d, lo = (%lld, %lld), hi=(%lld, %lld) \n ", me, A_read_lo[0], A_read_lo[1], A_read_hi[0], A_read_hi[1]);
  }

  A_rw_ld[0] = A_dist_hi[0] - A_dist_lo[0] + 2;
  A_rw_ld[1] = A_dist_hi[1] - A_dist_lo[1] + 2;
  NGA_Get64(g_A, A_read_lo, A_read_hi, A_read_data, A_rw_ld);
  if (me <= 1)
  {
    printf("Data read on rank %d: ", me);
    for (int k = 0; k < A_read_data_size; k++)
      printf(" %d, ", A_read_data[k]);
    printf("\n");
    fflush(stdout);
  }

  free(A_read_data);
  free(A_write_data);

  GA_Destroy(g_A);
  GA_Terminate();
  MPI_Finalize();
}
